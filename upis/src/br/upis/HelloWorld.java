package br.upis;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Define o tipo de conte�do da resposta
		response.setContentType("text/html");

		// Gera o conte�do HTML
		StringBuilder html = new StringBuilder();
		html.append("<html>");
		html.append("<head>");
		html.append("<title>Hello World</title>");
		html.append("</head>");
		html.append("<body>");
		html.append("<h1>Hello World!</h1>");
		
		// Obt�m o caminho da imagem
		String imagePath = request.getServletContext().getRealPath("/images/exemplo.jpg");

		// L� a imagem do arquivo
		try (InputStream in = Files.newInputStream(Paths.get(imagePath))) {
			// Adiciona a imagem ao HTML
			byte[] bytes = in.readAllBytes();
			String imageBase64 = java.util.Base64.getEncoder().encodeToString(bytes);
			html.append("<img src=\"data:image/jpeg;base64," + imageBase64 + "\" alt=\"imagem\"/>");
		}

		html.append("</body>");
		html.append("</html>");
		
		// Escreve o HTML na resposta
		response.getWriter().write(html.toString());
	}
}
