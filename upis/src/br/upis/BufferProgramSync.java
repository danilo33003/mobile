package br.upis;

public class BufferProgramSync {
    private static final int BUFFER_SIZE = 5;
    private static int[] buffer = new int[BUFFER_SIZE];

    public static void main(String[] args) {
        // Vers�o sincronizada
        ProdutorSync produtor = new ProdutorSync();
        ConsumidorSync consumidor = new ConsumidorSync();
        
        produtor.start();
        consumidor.start();
    }

    static class ProdutorSync extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (buffer) {
                    buffer[i] = i + 1;
                    System.out.println("Produtor adicionou: " + buffer[i]);
                }
            }
        }
    }

    static class ConsumidorSync extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (buffer) {
                    int valor = buffer[i];
                    System.out.println("Consumidor removeu: " + valor);
                }
            }
        }
    }
}
