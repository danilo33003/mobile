package br.upis;

public class Horario {
    private int segundos;

    public Horario(int horas, int minutos, int segundos) {
        this.segundos = horas * 3600 + minutos * 60 + segundos;
    }

    public int getHora() {
        return segundos / 3600;
    }

    public void setHora(int horas) {
        int minutos = getMinuto();
        int segundos = getSegundo();
        this.segundos = horas * 3600 + minutos * 60 + segundos;
    }

    public int getMinuto() {
        return (segundos % 3600) / 60;
    }

    public void setMinuto(int minutos) {
        int horas = getHora();
        int segundos = getSegundo();
        this.segundos = horas * 3600 + minutos * 60 + segundos;
    }

    public int getSegundo() {
        return segundos % 60;
    }

    public void setSegundo(int segundos) {
        int horas = getHora();
        int minutos = getMinuto();
        this.segundos = horas * 3600 + minutos * 60 + segundos;
    }

    public void incrementaSegundo() {
        segundos++;
        if (segundos > 86399) {
            segundos = 0;
        }
    }

    public void incrementaMinuto() {
        segundos += 60;
        if (segundos > 86399) {
            segundos %= 86400;
        }
    }

    public void incrementaHora() {
        segundos += 3600;
        if (segundos > 86399) {
            segundos %= 86400;
        }
    }

    public boolean ehUltimoSegundo() {
        return segundos == 86399;
    }

    public boolean ehPrimeiroSegundo() {
        return segundos == 0;
    }

    public void exibirEstado() {
        int horas = getHora();
        int minutos = getMinuto();
        int segundos = getSegundo();
        System.out.printf("Hor�rio: %02d:%02d:%02d%n", horas, minutos, segundos);
    }
}
