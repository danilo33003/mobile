package br.upis;

public class BufferProgramMain {
    private static final int BUFFER_SIZE = 5;
    private static int[] buffer = new int[BUFFER_SIZE];

    public static void main(String[] args) {
        // Vers�o n�o sincronizada
        Produtor produtor = new Produtor();
        Consumidor consumidor = new Consumidor();
        
        produtor.start();
        consumidor.start();
    }

    static class Produtor extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                buffer[i] = i + 1;
                System.out.println("Produtor adicionou: " + buffer[i]);
            }
        }
    }

    static class Consumidor extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int valor = buffer[i];
                System.out.println("Consumidor removeu: " + valor);
            }
        }
    }
}


